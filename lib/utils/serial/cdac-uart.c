

#include <sbi/riscv_io.h>
#include <sbi_utils/serial/cdac-uart.h>




volatile u32 *uart;



void cuart_putc(char ch)
{
	uint8_t lsr;

	uart[UART_DR] = ch;
	__asm__ __volatile__ ("fence"); //To complete the write operation in O-O-O processor

	do
	{
		lsr = uart[UART_LSR];
		lsr = lsr&0x20;
	}while(lsr != 0x20);

}

int cuart_getc(void)
{
	uint8_t lsr,Rx_char;

	lsr = uart[UART_LSR];
	lsr = lsr & 1;

	if(!lsr)			
		return -1;	

	Rx_char=uart[UART_DR];
	

	return Rx_char;
}




int cdac_uart_init(unsigned long base, u32 in_freq, u32 baudrate, u32 reg_shift,
		  u32 reg_width)
{

	uart 		   =(void*)(uintptr_t)(base);

	 uart[UART_IIR] = 1; 
	__asm__ __volatile__ ("fence");

	return 0;
}
