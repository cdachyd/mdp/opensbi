
# Compiler flags
platform-cppflags-y =
platform-cflags-y =
platform-asflags-y =
platform-ldflags-y =

# Command for platform specific "make run"
platform-runcmd = qemu-system-riscv$(PLATFORM_RISCV_XLEN) -M cdac -m 256M \
  -nographic -kernel $(build_dir)/platform/qemu/cdac/firmware/fw_payload.elf

# Common drivers to enable
PLATFORM_IRQCHIP_PLIC=n
PLATFORM_SERIAL_CDAC_UART=y
#PLATFORM_SERIAL_UART8250=y for QEMU TEST uncomment and comment above
PLATFORM_SYS_CLINT=y

# Blobs to build
FW_TEXT_START=0x80000000
FW_DYNAMIC=y
FW_JUMP=y
FW_JUMP_ADDR=0x80200000
FW_PAYLOAD=y
FW_PAYLOAD_OFFSET=0x200000
FW_PAYLOAD_FDT_ADDR=0x80800000
#FW_PAYLOAD_FDT_ADDR=0x82200000 fro QEMU TEST uncomment and comment above
PLATFORM_RISCV_ISA = rv64imafd
PLATFORM_INCLUDE_LIBC=y
