

#include <sbi/riscv_encoding.h>
#include <sbi/riscv_io.h>
#include <sbi/sbi_const.h>
#include <sbi/sbi_hart.h>
#include <sbi/sbi_console.h>
#include <sbi/sbi_platform.h>
#include <sbi/riscv_io.h>
#include <sbi_utils/irqchip/plic.h>
#include <sbi_utils/serial/cdac-uart.h>
#include <sbi_utils/sys/clint.h>

#include <sbi_utils/serial/uart8250.h>

/* clang-format off */

#define CDAC_HART_COUNT			1
#define CDAC_HART_STACK_SIZE		8192

#define CDAC_CLINT_ADDR			0x20010000 // 0x2000000 Clint Address in QEMU

#define CDAC_PLIC_NUM_SOURCES		2

#define CDAC_UART16550_ADDR		0x10000100 // 0xf00001000 UART Address in QEMU
#define CDAC_UART_BAUDRATE		38400      // Not used as of now
#define CDAC_UART_SHIFTREG_ADDR		1843200    // Not used as of now



/** Default feature set for CDAC OOO(out of order) platform HART -> NO PMP YET*/

#define SBI_CDAC_PLATFORM_DEFAULT_FEATURES                                \
	(SBI_PLATFORM_HAS_TIMER_VALUE |       \
	 SBI_PLATFORM_HAS_SCOUNTEREN | SBI_PLATFORM_HAS_MCOUNTEREN | \
	 SBI_PLATFORM_HAS_MFAULTS_DELEGATION)




static int CDAC_final_init(bool cold_boot)
{

	return 0;
}

static u32 CDAC_pmp_region_count(u32 hartid)
{
	return 0;
}

static int CDAC_pmp_region_info(u32 hartid, u32 index, ulong *prot, ulong *addr,
				ulong *log2size)
{
	int ret = 0;

	switch (index) {
	case 0:
		*prot	= PMP_A_NAPOT | PMP_R | PMP_W | PMP_X;
		*addr	  = 0;
		*log2size = __riscv_xlen;
		break;
	default:
		ret = -1;
		break;
	};

	return ret;
}

static int cdac_console_init(void)
{
	
	return cdac_uart_init(CDAC_UART16550_ADDR, 0,0, 0, 1);  // UART init has been done in previous booting stage
	//return uart8250_init(CDAC_UART16550_ADDR, CDAC_UART_SHIFTREG_ADDR,
			     //CDAC_UART_BAUDRATE, 0, 1);

}

static int CDAC_irqchip_init(bool cold_boot)
{
	return 0;
}

static int CDAC_ipi_init(bool cold_boot)
{
	int rc;
	if (cold_boot) {
		rc = clint_cold_ipi_init(CDAC_CLINT_ADDR+0x400, CDAC_HART_COUNT);
		if (rc)
			return rc;
	}



	return clint_warm_ipi_init();
}

static int CDAC_timer_init(bool cold_boot)
{
	int rc;

	if (cold_boot) {
		rc = clint_cold_timer_init(CDAC_CLINT_ADDR, CDAC_HART_COUNT,TRUE);
		if (rc)
			return rc;
	}

	return clint_warm_timer_init();
}

static int CDAC_system_down(u32 type)
{

	return 0;
}

const struct sbi_platform_operations platform_ops = {
	.pmp_region_count	= CDAC_pmp_region_count,
	.pmp_region_info	= CDAC_pmp_region_info,
	.final_init		= CDAC_final_init,
	.console_putc		= cuart_putc, //uart8250_putc, for QEMU Test//
	.console_getc		=  cuart_getc,//uart8250_getc, for QEMU Test//
	.console_init		= cdac_console_init,
	.irqchip_init		= CDAC_irqchip_init,
	.ipi_send		= clint_ipi_send,
	.ipi_clear		= clint_ipi_clear,
	.ipi_init		= CDAC_ipi_init,
	.timer_value		= clint_timer_value,
	.timer_event_stop	= clint_timer_event_stop,
	.timer_event_start	= clint_timer_event_start,
	.timer_init		= CDAC_timer_init,
	.system_reboot		= CDAC_system_down,
	.system_shutdown	= CDAC_system_down
};

const struct sbi_platform platform = {
	.opensbi_version	= OPENSBI_VERSION,
	.platform_version	= SBI_PLATFORM_VERSION(0x0, 0x01),
	.name			= "CDAC Machine",
	.features		= SBI_CDAC_PLATFORM_DEFAULT_FEATURES,
	.hart_count		= CDAC_HART_COUNT,
	.hart_stack_size	= CDAC_HART_STACK_SIZE,
	.disabled_hart_mask	= 0,
	.platform_ops_addr	= (unsigned long)&platform_ops
};
