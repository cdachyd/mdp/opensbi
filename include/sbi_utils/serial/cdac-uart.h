
#ifndef __SERIAL_CDAC_UART_H__
#define __SERIAL_CDAC_UART_H__

#include <sbi/sbi_types.h>


#define    UART_DR 	0x0
#define    UART_IE  	0x1
#define    UART_IIR 	0x2
#define    UART_LCR 	0x3
#define    Dummy10 	0x4
#define    UART_LSR 	0x5




void cuart_putc(char ch);

int cuart_getc(void);


int cdac_uart_init(unsigned long base, u32 in_freq, u32 baudrate, u32 reg_shift,u32 reg_width);

#endif
